package com.xeven.hamid.yahyasports;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    HashMap<String, Integer> HashMapForFiles;
    SliderLayout sliderLayout;
    List<Product> products;
    private RecyclerView recycleViewSpecial,recycleViewFeatured;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private HomeAdapter homeAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        sliderLayout = view.findViewById(R.id.slider);
        AddImagesURLFile();
        products=new ArrayList<>();


        for (String name : HashMapForFiles.keySet()) {

            TextSliderView textSliderView = new TextSliderView(getContext());

            textSliderView
                    .image(HashMapForFiles.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());

            textSliderView.getBundle()
                    .putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.DepthPage);
//        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(this);

         recycleViewSpecial= view.findViewById(R.id.special_product_recycleview);
         recycleViewFeatured= view.findViewById(R.id.featured_product_recycleview);

        // MainActivity.setHeader("Categories",1);

        // list is full.
        // GetCategory();

        homeAdapter= new HomeAdapter( products,getChildFragmentManager());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1,GridLayoutManager.HORIZONTAL,false);


        recycleViewSpecial.setLayoutManager(mLayoutManager);
        recycleViewSpecial.setItemAnimator(new DefaultItemAnimator());
        recycleViewSpecial.setAdapter(homeAdapter);

        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(getContext(), 2);
        recycleViewFeatured.setLayoutManager(mLayoutManager1);
        recycleViewFeatured.setItemAnimator(new DefaultItemAnimator());
        recycleViewFeatured.setAdapter(homeAdapter);
        Data();

        return view;
    }
    public void Data(){
        String[] links={"https://3.imimg.com/data3/HB/XA/MY-2676533/mwp-27-250x250.jpg"};

        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        homeAdapter.notifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    public void AddImagesURLFile() {

        HashMapForFiles = new HashMap<>();
        HashMapForFiles.put("CupCake",
                R.drawable.slide1);
        HashMapForFiles.put("BuntyCake",
                R.drawable.slide2);
        HashMapForFiles.put("ChocalateCake",
                R.drawable.slide3);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
