package com.xeven.hamid.yahyasports;

/**
 * Created by hamid on 10/10/2017.
 */

public class Product {
    private String id;
    private String name;
    private String type;
    private String description;
    private String[] images=new String[5];
    private String[] comments=new String[5];

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String[] getComments() {
        return comments;
    }

    public void setComments(String[] comments) {
        this.comments = comments;
    }

    public Product(String id, String name, String type, String description, String[] images, String[] comments) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.images = images;
        this.comments = comments;
    }

    public Product() {
    }
}
