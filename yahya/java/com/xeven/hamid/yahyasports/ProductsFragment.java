package com.xeven.hamid.yahyasports;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hamid on 10/10/2017.
 */

public class ProductsFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    HashMap<String, Integer> HashMapForFiles;

    List<Product> products;
    private RecyclerView recycleView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private HomeFragment.OnFragmentInteractionListener mListener;
    private HomeAdapter homeAdapter;

    public ProductsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductsFragment newInstance(String param1, String param2) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_products, container, false);

        products=new ArrayList<>();




        recycleView= view.findViewById(R.id.product_recycleview);


        homeAdapter= new HomeAdapter( products,getChildFragmentManager());


        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(getContext(), 2);
        recycleView.setLayoutManager(mLayoutManager1);
        recycleView.setItemAnimator(new DefaultItemAnimator());
        recycleView.setAdapter(homeAdapter);
        Data();

        return view;
    }
    public void Data(){
        String[] links={"https://3.imimg.com/data3/HB/XA/MY-2676533/mwp-27-250x250.jpg"};

        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        products.add(new Product("1","Dumbles","","",links,links));
        homeAdapter.notifyDataSetChanged();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
