package com.xeven.hamid.yahyasports;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ResetPassword extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getSupportActionBar().hide();

    }
    public void onResetclick(View view){
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}
