package com.xeven.hamid.yahyasports;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

    }
    public void onLoginclick(View view){
        Intent intent=new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
    public void onRegisterclick(View view){
        Intent intent=new Intent(this,Registration.class);
        startActivity(intent);
    }
    public void onForgotPasswordclick(View view){
        Intent intent=new Intent(this,ForgotPassword.class);
        startActivity(intent);
    }
}
