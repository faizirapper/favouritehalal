package com.xeven.hamid.yahyasports;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by faizi on 10/10/2017.
 */

class CatalogPagerAdapter extends FragmentPagerAdapter {

    private int tabCount;

    CatalogPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return CatalogSlider.newInstance("", "");
            case 1:
                return Description.newInstance("","");
            case 2:
                return Comments.newInstance("", "");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position){

        switch(position){
            case 0:
                return "Catalog";
            case 1:
                return "Description";
            case 2:
                return "Comments";
        }
        return null;
    }

}
