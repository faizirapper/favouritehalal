package com.xeven.hamid.yahyasports;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * Created by hamid on 10/10/2017.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    List<Product> products;
    Context context;
    FragmentManager childFragmentManager;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address, desc,paid;
        public RatingBar ratingBar;
        public Button button;
        ImageView imageView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R .id.tv_title);
            address = (TextView) view.findViewById(R.id.tv_comments);
            desc = (TextView) view.findViewById(R.id.tv_added_by);
            paid = (TextView) view.findViewById(R.id.tv_added_on);
            button=(Button)view.findViewById(R.id.catalogButton);
            imageView=(ImageView)view.findViewById(R.id.list_image) ;
            context=view.getContext();

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }


    public HomeAdapter(List<Product> products, FragmentManager fm) {
        this.products = products;
        this.childFragmentManager=fm;
    }

    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item_layout, parent, false);

        return new HomeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeAdapter.MyViewHolder holder, int position) {
        Product products = this.products.get(position);
        holder.name.setText(products.getName());

        holder.desc.setText(products.getDescription());

        Glide.with(context).load(this.products.get(position).getImages()[0]).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);

        //if(job.getRate().equals("0")||job.getRate().equals(""))

    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
