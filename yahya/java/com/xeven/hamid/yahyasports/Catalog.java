package com.xeven.hamid.yahyasports;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Catalog extends Fragment implements TabLayout.OnTabSelectedListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private TabLayout tabLayout;
    private ViewPager viewpager;


    public Catalog() {
        // Required empty public constructor
    }


    public static Catalog newInstance(String param1, String param2) {
        Catalog fragment = new Catalog();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_catalog, container, false);

        tabLayout = view.findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewpager = view.findViewById(R.id.pager);

        CatalogPagerAdapter catalogPagerAdapter = new CatalogPagerAdapter(getChildFragmentManager(), tabLayout.getTabCount());

        viewpager.setAdapter(catalogPagerAdapter);
        tabLayout.setOnTabSelectedListener(this);
        tabLayout.setTabTextColors(Color.parseColor("#222222"), Color.parseColor("#fc8395"));
        tabLayout.setupWithViewPager(viewpager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#000000"));
        viewpager.setOffscreenPageLimit(tabLayout.getTabCount());

//        getChildFragmentManager().beginTransaction().
//                add(R.id.childcontainer, CatalogSlider.newInstance("","")).commit();

        return view;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
